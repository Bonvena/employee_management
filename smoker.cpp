#include "smoker.h"
#include <iostream>
using namespace std;

Smoker::Smoker(int timestampValue, int locationValue)
{
	timestamp = timestampValue;
	location = locationValue;
}

int Smoker::getTimestamp()
{
	return timestamp;
}

int Smoker::getLocation()
{
	return location;
}
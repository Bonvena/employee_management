#include "report.h"
#include "buddy.h"

#include <fstream>
#include <iostream>
#include <vector>
using namespace std;

void menu()
{
	cout << "Main menu:\n"
		 << "a - Report slackers.\n"
		 << "b - Report suspected smokers.\n"
		 << "c - Report employee's friends.\n"
		 << "d - Identify broken tracking devices.\n"
		 << "e - Exit\n";
}


//Assumes that an employee only has one record per timestamp, otherwise they would be in two places at once.
void reportSlackers(vector<Employee> &infoVector, vector<Tracking> &trackVector, int timestampCount)
{
	int office;
	int employeeId;
	double slackerCount = 0;
	double slackerRatio = 0;
	cout << "Slackers:\n";
	for(int i = 0; i < infoVector.size(); ++i) //For each employee record go through each timestamp
	{
		employeeId = infoVector[i].getId();
		office = infoVector[i].getOffice();
		
		for(int k = 0; k < trackVector.size(); ++k)
		{
			if((trackVector[k].getId() == employeeId) && (trackVector[k].getLocation() != office))//if tracking record of employee does not show employee was at desk
			{
				slackerCount++;
			}
		}
		
		slackerRatio = slackerCount / timestampCount;
		if(slackerRatio > 0.25)
		{
			cout << infoVector[i].getName() << endl;
		}
		slackerCount = 0;
	}
}

void reportSmokers(vector<Employee> &infoVector, vector<Tracking> &trackVector, vector<Smoker> &smokeVector, int timestampCount)
{
	int office, employeeId, trackTimestamp, trackLocation;
	double smokeCount = 0;
	double smokeRatio = 0;
	
	for(int i = 0 ; i < infoVector.size(); ++i)//for each employee
	{
		employeeId = infoVector[i].getId();
		office = infoVector[i].getOffice();
		
		for(int k = 0; k < trackVector.size(); ++k) //looks through every tracking record
		{
			if(trackVector[k].getId() == employeeId)//if tracking record is employee's record, retain timestamp and location
			{
				trackTimestamp = trackVector[k].getTimestamp();
				trackLocation = trackVector[k].getLocation();
				
				for(int j = 0; smokeVector[j].getTimestamp() <= trackTimestamp; ++j) //for each smoke record with a timestamp of less or equal to tracking record's timestamp (smokeVector[j].timestamp <= timestamp or j < smokeVector.size())
				{
					if((trackTimestamp == smokeVector[j].getTimestamp()) && (trackLocation == smokeVector[j].getLocation()))//if a smoke record's timestamp and location match the tracking record of an employee
					{
						smokeCount++;
						
						//cout << "Found Smoker: " << smokeCount << "Id number: " << employeeId << endl << "Tracking time stamp: " << trackTimestamp << "Smoke Time stamp: " << smokeVector[j].timestamp << endl << "Track Location: " << trackLocation << "Smoke Vector Location: " << smokeVector[j].location << endl;
					}
				}
			}
		}
		
		smokeRatio = smokeCount / timestampCount;
		//cout << employeeId << " " << smokeCount << " " << timestampCount << " " << smokeRatio << endl;
		
		if(smokeRatio >= 0.01)
		{
			cout << "Found smoker: " << infoVector[i].getName() << endl;
		}
		smokeCount = 0;

	}
}

void reportFriends(vector<Employee> &infoVector, vector<Tracking> &trackVector)
{
	string forename, surname;
	int buddyId = 0;
	int employeeId, office;
	
	
	cout << "Enter the employee's first name (case sensitive): ";
	cin >> forename;
	cout << "\nEnter the employee's last name (case sensitive): ";
	cin >> surname;
	
	
	string name = forename + " " + surname;
	bool foundEmployee = false;
	
	
	for(int i = 0; i < infoVector.size(); ++i)
	{
		if(name == infoVector[i].getName())
		{
			employeeId = infoVector[i].getId();
			office = infoVector[i].getOffice();
			foundEmployee = true;
			break;
		}
	}
	
	if(false == foundEmployee)
	{
		cerr << "Employee not found\n";
		return;
	}
	
	cout << "Employee's top ten friends:\n";
	vector<Buddy> friends; //friends of employee
	
	for(int k = 0; k < trackVector.size(); ++k)//for each timestamp
	{
		if(employeeId == trackVector[k].getId())//if timestamp's id matches employee's id
		{
			
				for(int index = 0; index < trackVector.size(); ++index)//look at every tracking record timestamp no greater than the original employee's timestamp
				{
					if((trackVector[k].getTimestamp() == trackVector[index].getTimestamp()) && (trackVector[k].getLocation() == trackVector[index].getLocation()) && (employeeId != trackVector[index].getLocation()))//if another employee's timestamp and location match that of the one searched for, they are friends
					{
						int friendOffice = 0000;
						string friendName;
						for(int i = 0; i < infoVector.size(); ++i)
						{
							if(trackVector[index].getId() == infoVector[i].getId())
							{
								friendOffice = infoVector[i].getOffice();
								friendName = infoVector[i].getName();
								break;
							}
						}
						
						if(friendOffice != office)
						{
							buddyId = trackVector[index].getId();
							
							bool foundFriend = false;
							
							for(int z = 0; z < friends.size(); ++z) //go through records of employee's friends
							{
								if(buddyId == friends[z].getId()) //if friend already there, increase count
								{
									friends[z].addFriendCount();
									foundFriend = true;
									//cout << "Friend Count for id" << buddyId << " " << friendName << " = "  << friends[z].friendCount << endl;
									
									break;
								}
							}
							
							if(false == foundFriend)
							{
								Buddy pal(buddyId);
								friends.push_back(pal);
								//cout << "Friend Count = 1 for id" << buddyId <<  " " << friendName << endl;
							}
						}
					}
				}
		}
	}
	
	/*
	for(int i = 0; i < friends.size(); ++i)
	{
		for(int j = 0; j < infoVector.size(); ++j)
		{
			if(friends[i].id == infoVector[j].id)
			{
				cout << infoVector[j].name << endl;
				cout << "Count: " << friends[i].friendCount << endl;
				cout << endl;
				break;
			}
		}
	}
	*/

	for(int i = 0; i < friends.size(); ++i) //sorts friend count in descending order
	{
		for(int j = (i+1); j < friends.size(); ++j)
		{
			if(friends[i].getFriendCount() < friends[j].getFriendCount()) //swaps
			{
				Buddy temp(friends[i].getId(), friends[i].getFriendCount());
				friends[i] = friends[j];
				friends[j] = temp;
			}
		}
	}
	
	for(int k = 0; k < 10 && k < friends.size(); k++) //connects id in friends vector with name in employee vector
	{
		for(int j = 0; j < infoVector.size(); ++j)
		{
			if(friends[k].getId() == infoVector[j].getId())
			{
				cout << infoVector[j].getName() << endl;
				break;
			}
		}
	}
	
	if(0 == friends.size())
	{
		cout << name << " has no friends in this company.\n";
	}
}

void reportBroken(vector<Employee> &infoVector, vector<Tracking> &trackVector, int timestampValue)
{
	int employeeId;
	int trackVectorSize = trackVector.size();
	double found = 0;
	double foundRatio = 0;
	
	cout << "Employees with Broken Tracking Devices" << endl;
	
	for(int i = 0; i < infoVector.size(); ++i) //for each employee
	{
		employeeId = infoVector[i].getId();
		
		for(int k = trackVector[0].getTimestamp(); k <= trackVector[trackVectorSize-1].getTimestamp(); ++k)//for each timestamp value
		{
			for(int j = 0; j < trackVector.size(); ++j)//looks through every tracking record
			{
				if((k == trackVector[j].getTimestamp()) && (employeeId == trackVector[j].getId()))//if a timestamp matches an employee timestamp and the employee is the one in question
				{
					found += 1; //there is a record for that employee at that timestamp
					break;
				}
			}
			
		}
		
		foundRatio = found / timestampValue;
		
		//cout << found << " " << timestampValue << " " << foundRatio << endl;
		
		if(foundRatio < 0.95)
		{
			cout << infoVector[i].getName() << endl;
		}
		
		found = 0;
		
	}
}

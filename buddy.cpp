#include "buddy.h"
#include <iostream>
using namespace std;

Buddy::Buddy(int idValue)
{
	id = idValue;
	friendCount = 1;
}

Buddy::Buddy(int idValue, int friendCountValue)
{
	id = idValue;
	friendCount = friendCountValue;
}

int Buddy::getId()
{
	return id;
}

int Buddy::getFriendCount()
{
	return friendCount;
}

void Buddy::addFriendCount()
{
	friendCount += 1;
}


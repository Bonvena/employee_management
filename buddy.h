#ifndef BUDDY_H
#define BUDDY_H

#include <iostream>
using namespace std;

//used in determining employee's friends
class Buddy
{
public:
	Buddy(int idValue);
	Buddy(int idValue, int friendCountValue);
	
	int getId();
	int getFriendCount();
	void addFriendCount();
	
private:
	int id;
	int friendCount;
};

#endif


#include<cstdlib>
#include<iostream>
#include<fstream>
#include<string>
#include<vector>

#include "employee.h"
#include "tracking.h"
#include "smoker.h"
#include "buddy.h"
#include "populate.h"
#include "report.h"

using namespace std;


int main(int argc, char * argv[]){

	if (argc != 4)
	{
		cerr << "This program requires 3 parameters, such as: \n";
		cerr << " ./program employee.dat tracking.dat smoke.dat\n";
		exit(1);
	}
	
	ifstream employeeStream(argv[1]);
	if(employeeStream.fail())
	{
		cerr << "Error opening first parameter, employee data." << endl;
		exit(1);
	}

	ifstream trackStream(argv[2]);
	if(trackStream.fail())
	{
		cerr << "Error opening second parameter, tracking data." << endl;
		exit(1);
	}

	ifstream smokeStream(argv[3]);
	if(smokeStream.fail())
	{
		cerr << "Error opening third parameter, smoke data." << endl;
		exit(1);
	}
	
	vector<Employee> infoVector;
	vector<Tracking> trackVector;
	vector<Smoker> smokeVector;
	
	populateEmployees(employeeStream, infoVector);
	int timestampCount = populateTracking(trackStream, trackVector);
	populateSmoker(smokeStream, smokeVector);
	
	menu();
	
	char input = cin.get();
	do{
		if ('a' == input || 'A' == input)
		{
			reportSlackers(infoVector, trackVector, timestampCount);
		}
		else if('b' == input || 'B' == input)
		{
			reportSmokers(infoVector, trackVector, smokeVector, timestampCount);
		}
		else if ('c' == input || 'C' == input)
		{
			reportFriends(infoVector, trackVector);
		}
		else if('d' == input || 'D' == input)
		{
			reportBroken(infoVector, trackVector, timestampCount);
		}
		else if('e' == input || 'E' == input)
		{
			exit(1);
		}
		else
		{
			menu();
		}
		
		input = cin.get();
	} while (input != 'e');
	
return 0;
}


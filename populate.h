#ifndef POPULATE_H
#define POPULATE_H

#include <vector>
#include <fstream>
#include "employee.h"
#include "tracking.h"
#include "smoker.h"

using namespace std;

//Populates Employee Database Vector
void populateEmployees(ifstream &employeeStream, vector<Employee> &infoVector);

//Populates Tracking Database Vector
int populateTracking(ifstream &trackStream, vector<Tracking> &trackVector);

////Populates Smoker Database Vector
void populateSmoker(ifstream &smokeStream, vector<Smoker> &smokeVector);

#endif
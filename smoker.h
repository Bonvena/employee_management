#ifndef SMOKER_H
#define SMOKER_H

#include <iostream>
using namespace std;

//works with smoker.dat
class Smoker
{
public:
	Smoker(int timestampValue, int locationValue);
	
	int getTimestamp();
	int getLocation();
	
private:
	int timestamp;
	int location;
};

#endif
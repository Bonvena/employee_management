#ifndef REPORT_H
#define REPORT_H

#include <vector>
#include "employee.h"
#include "tracking.h"
#include "smoker.h"

using namespace std;

//prints menu
void menu();

//Reports slackers who spend more than 25% of the time away from desk
void reportSlackers(vector<Employee> &infoVector, vector<Tracking> &trackVector, int timestampCount);

//Reports those who spent at least 1% of the time in rooms where smoke was detected
void reportSmokers(vector<Employee> &infoVector, vector<Tracking> &trackVector, vector<Smoker> &smokeVector, int timestampCount);

//Reports top ten friends of an employee provided by the user
void reportFriends(vector<Employee> &infoVector, vector<Tracking> &trackVector);

//Reports devices which report at less than 95% of timestamps
void reportBroken(vector<Employee> &infoVector, vector<Tracking> &trackVector, int timestampValue);

#endif
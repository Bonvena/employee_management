
all: program

program: management.o employee.o tracking.o smoker.o buddy.o populate.o report.o
	g++ -o program management.o employee.o tracking.o smoker.o buddy.o populate.o report.o

management.o: management.cpp employee.h tracking.h smoker.h buddy.h populate.h report.h
	g++ -c management.cpp

report.o: report.cpp report.h employee.h tracking.h smoker.h buddy.h
	g++ -c report.cpp

populate.o: populate.cpp populate.h employee.h tracking.h smoker.h
	g++ -c populate.cpp

buddy.o: buddy.cpp buddy.h
	g++ -c buddy.cpp

smoker.o: smoker.cpp smoker.h
	g++ -c smoker.cpp

tracking.o: tracking.cpp tracking.h
	g++ -c tracking.cpp

employee.o: employee.cpp employee.h
	g++ -c employee.cpp
	
clean:
	- rm -rf program *.o 

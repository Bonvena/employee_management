#include "tracking.h"
#include <iostream>
using namespace std;

Tracking::Tracking(int timestampValue, int idValue, int locationValue)
{
	timestamp = timestampValue;
	id = idValue;
	location = locationValue;
}

int Tracking::getTimestamp()
{
	return timestamp;
}

int Tracking::getId()
{
	return id;
}

int Tracking::getLocation()
{
	return location;
}

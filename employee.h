#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>
using namespace std;

//Works with employee.dat
class Employee
{
	
public:
	Employee(int idValue, string nameValue, int officeValue);
	
	int getId();
	string getName();
	int getOffice();
	
private:
	int id;
	string name;
	int office;
};

#endif

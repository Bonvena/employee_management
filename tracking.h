#ifndef TRACKING_H
#define TRACKING_H
#include <iostream>
using namespace std;

//works with tracking.dat
class Tracking
{
	
public:
	Tracking(int timestampValue, int idValue, int locationValue);
	
	int getTimestamp();
	int getId();
	int getLocation();
	
private:
	int timestamp;
	int id;
	int location;
	
};


#endif
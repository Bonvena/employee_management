#include "employee.h"

Employee::Employee(int idValue, string nameValue, int officeValue)
{
	id = idValue;
	name = nameValue;
	office = officeValue;
}

int Employee::getId()
{
	return id;
}

int Employee::getOffice()
{
	return office;
}

string Employee::getName()
{
	return name;
}
#include "populate.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

void populateEmployees(ifstream &employeeStream, vector<Employee> &infoVector)
{
	string s, name, forename, surname;
	int number, office;
	
	getline(employeeStream, s);
	getline(employeeStream, s);
	
	int count = atoi(s.c_str());
	
	getline(employeeStream, s);
	for(int i = 0; i < count; ++i)
	{
		employeeStream >> number;
		employeeStream >> forename;
		employeeStream >> surname;
		name = forename + " " + surname;
		employeeStream >> office;
		
		Employee worker(number, name, office);
		infoVector.push_back(worker);
	}
	
}

int populateTracking(ifstream &trackStream, vector<Tracking> &trackVector)
{
	string s;
	int timestamp, employeeId, location;
	
	getline(trackStream, s);
	getline(trackStream, s);

	int count = atoi(s.c_str());
	getline(trackStream, s);
	
	for(int i = 0; i < count; ++i)
	{
		trackStream >> timestamp;
		trackStream >> employeeId;
		trackStream >> location;
		
		Tracking tracker(timestamp, employeeId, location);
		trackVector.push_back(tracker);
	}
	return (trackVector[count-1].getTimestamp() - trackVector[0].getTimestamp()) + 1;

}

void populateSmoker(ifstream &smokeStream, vector<Smoker> &smokeVector)
{
	string s;
	int timestamp, location;
	
	getline(smokeStream, s);
	getline(smokeStream, s);
	
	int count = atoi(s.c_str());
	getline(smokeStream, s);
	
	for(int i = 0; i < count; ++i)
	{
		smokeStream >> timestamp;
		smokeStream >> location;
		
		Smoker smokeRecord(timestamp, location);
		smokeVector.push_back(smokeRecord);
	}
	
}
